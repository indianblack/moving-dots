![Dots](./src/assets/images/dots-image.png)

# Projekt II - Animacja obiektów oscylujących w przestrzeni

Aplikacja napisana w [three.js](https://threejs.org/), przedstawiająca kule o ustalonym rozmiarze przysuwane i odsuwane od kamery. Przykładową animację można zobaczyć [tutaj](https://processing.org/examples/sine.html).

**Student:** Karolina Czochańska 

**Przedmiot**: Grafika komputerowa

**Uczelnia**: Wyższa Szkoła Ekonomiczna w Białymstoku

**Data:** czerwiec 2021

## Uruchamianie
1. Sklonuj repozytorium
2. Zainstaluj [Node.js](https://nodejs.org/en/) aby móc użyć pozostałych komend
3. Zainstaluj zależności: npm install. Tą komendę wystarczy wywołać tylko raz.
4. Odpal lokalny serwer deweloperski: npm run dev
5. Zbuduj wersję produkcyjną: npm run build

## Przydatne linki

**Tree.js**
- https://threejs.org/docs/index.html#manual/en/introduction/Creating-a-scene
- https://threejs.org/docs/#api/en/geometries/SphereGeometry
- https://threejs.org/examples/#webgl_loader_mdd
- https://threejsfundamentals.org/threejs/lessons/threejs-primitives.html
- https://threejs.org/examples/#webgl_loader_gltf_transmission
- https://threejs.org/examples/#webgl_materials
- https://threejs.org/examples/#webgl_camera_array

**Inne**
- https://processing.org/examples/sine.html
- https://processing.org/examples/sinewave.html
- https://favicon.io/
- https://www.toptal.com/developers/gitignore
- https://stackoverflow.com/questions/8253880/three-js-only-one-sphere-is-visible
- https://subscription.packtpub.com/book/web_development/9781783981182/1/ch01lvl1sec16/controlling-the-variables-used-in-the-scene
- https://stackoverflow.com/questions/18357529/threejs-remove-object-from-scene
- https://stackoverflow.com/questions/51784301/delete-an-object-from-scene-in-three-js
- https://stackoverflow.com/questions/19426559/three-js-access-scene-objects-by-name-or-id

## Licencja
[MIT](https://choosealicense.com/licenses/mit/)