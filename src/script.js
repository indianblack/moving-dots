import "./style.css";
import * as THREE from "three";
import * as dat from "dat.gui";

// Canvas
const canvas = document.querySelector("canvas.webgl");

// Scene
const scene = new THREE.Scene();

// Background
scene.background = new THREE.Color(0xf9f1f1);

/**
 * GUI
 */

const params = {
  numberOfDots: 1,
};

let numberOfDots = 1;

// GUI controls
const gui = new dat.GUI();

gui
  .add(params, "numberOfDots", 1, 3, 1)
  .name("Ilość piłek")
  .onChange(function (number) {
    numberOfDots = number;
  });

/**
 * Objects
 */

// Sphere object
const radius = 2;
const widthSegments = 30;
const heightSegments = 30;
const geometry = new THREE.SphereGeometry(
  radius,
  widthSegments,
  heightSegments
);

// Mesh with material
let spheres = [];
let position = 0;
const initialNumberOfDots = 3;

for (let x = 0; x < initialNumberOfDots; x++) {
  const material = new THREE.MeshLambertMaterial({
    color: Math.random() * 0xffffff,
  });
  const sphere = new THREE.Mesh(geometry, material);
  sphere.name = "sphere" + x;
  sphere.position.x = position;
  sphere.castShadow = true;
  position += 12;
  scene.add(sphere);
  spheres.push(sphere);
}

/**
 * Lights
 */

const light = new THREE.DirectionalLight();
light.position.set(0.5, 0.5, 4);
light.castShadow = true;
light.shadow.camera.zoom = 5;
scene.add(light);

/**
 * Sizes
 */

const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  // Update camera
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  // Update renderer
  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

/**
 * Camera
 */

// Base camera
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
);
camera.position.x = 12; // position x
camera.position.y = 0; // position y
camera.position.z = 25; // camera zoom
scene.add(camera);

/**
 * Renderer
 */

const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
});
renderer.setClearColor(0x17293a);
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.shadowMap.enabled = true;

/**
 * Animate
 */

let count = 0;

function showDots() {
  var sphere1 = scene.getObjectByName("sphere0");
  var sphere2 = scene.getObjectByName("sphere1");
  var sphere3 = scene.getObjectByName("sphere2");

  switch (numberOfDots) {
    case 1:
      sphere2.visible = false;
      sphere3.visible = false;
      camera.position.x = 0;
      break;
    case 2:
      sphere2.visible = true;
      sphere3.visible = false;
      camera.position.x = 6;
      break;
    case 3:
      sphere1.visible = true;
      sphere2.visible = true;
      sphere3.visible = true;
      camera.position.x = 12;
      break;
  }
}

const drawFrame = () => {
  // Scale and move dots
  spheres.forEach(function (sphere, i) {
    let offset = (i * Math.PI) / 2;
    sphere.scale.x =
      sphere.scale.y =
      sphere.scale.z =
        Math.sin(count * 2 + offset) + 1.5;
    sphere.position.z = Math.sin(count * 2 + offset) * 3;
  });

  count += 0.01;

  // Change number of dots visible on the screen
  showDots();

  // Render
  renderer.render(scene, camera);

  // Call to animate next frame
  window.requestAnimationFrame(drawFrame);
};

drawFrame();
